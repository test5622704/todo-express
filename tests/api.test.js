const request = require('supertest');
const app = require('../index');

afterAll((done) => {
  app.close(done); // Schließe den Server nach Abschluss der Tests
});

describe('GET /tasks', () => {
  it('responds with json containing empty array', async () => {
    const response = await request(app).get('/tasks');
    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual([]);
  });
});

describe('POST /tasks', () => {
  it('responds with json containing the created task', async () => {
    const response = await request(app)
      .post('/tasks')
      .send({ task: 'New Task' });
    expect(response.statusCode).toBe(201);
    expect(response.body).toHaveProperty('task', 'New Task');
  });
});

describe('PUT /tasks', () => {
  it('responds with json containing the updated task', async () => {
    const response = await request(app)
      .put('/tasks/1')
      .send({ task: 'Updated Task' });
    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty('task', 'Updated Task');
  });
});

describe('DELETE /tasks', () => {
  it('responds with status 204 (No Content)', async () => {
    const response = await request(app).delete('/tasks/1');
    expect(response.statusCode).toBe(204);
  });
});

